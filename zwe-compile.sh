#!/bin/bash

# zwe.sh, Helper for the Zero Width Encoding to compile some tables.
# Copyright (C) 2023 Slatian
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

set -e
export LANG=C

show_help() {
cat <<EOF
Usage: zwe-comple.sh [table|awk_tables]
EOF
}

tokens=(0 1 2 "\n" 3 4 5 6 7 8 9 "/")
tokens+=(a b c d e f g h i j k l m n o p q r s t u v w x y z)
tokens+=(+ _ - . : "=" " " ! ? "#")

#factorial exponent
function factexp() {
	if [[ "$1" -le 0 ]] ; then
		printf "0\n"
	else
		printf "%d\n" "$(($2 ** $1 + $( factexp "$(( $1-1 ))" "$2" ) ))"
	fi
}

function factexp_rev() {
	n=0
	i=-1
	while [[ "$n" -le "$1" ]] ; do
		i="$((i+1))"
		n="$(factexp "$i" "$2")"
	done
	printf "%d\n" "$i"
}


function to_jnz() {
	n="$2"
	c=0
	if [[ -z "$n" ]] ; then
		n="$(factexp_rev "$1" 4)"
		c="$(factexp "$((n-1))" 4)"
		#printf "Places: %d  Correction: -%d\n" "$n" "$c"
	fi
	if [[ "$n" -gt 1 ]] ; then
		remain="$((($1-c)/4))"
		to_jnz "$remain" "$((n-1))"
	fi
	case "$((($1-c)%4))" in
		0)
			printf "j" ;;
		1)
			printf "n" ;;
		2)
			printf "w" ;;
		3)
			printf "z" ;;
	esac
	return 0
}

function compile_table_for_documentation() {
	printf "| #   | Encoded | Char  |\n"
	printf "|----:|--------:|-------|\n"
	for i in {0..48} ; do
		jnz="$(to_jnz "$i")"
		token="${tokens[$i]}"
		if [[ "$token" = " " ]] ; then token="SPACE" ; fi
		printf "| %3d | \`%5s\` | %-5s |\n" "$i" "$jnz" "$token"
	done
}

function compile_awk_tables() {
	awk_enc_table=""
	awk_dec_table=""

	# compile translation tables
	i=0
	for t in "${tokens[@]}" ; do
		jnz="$(to_jnz $i)"
		#echo "-> $t ($i) $jnz"
		awk_enc_table="$awk_enc_table enc_t[\"$t\"] = \"$jnz\";"
		awk_dec_table="$awk_dec_table dec_t[\"$jnz\"] = \"$t\";"
		i=$((i+1))
	done
	printf "Encoder table:\n%s\n\nDecoder table:\n%s\n" "$awk_enc_table" "$awk_dec_table"
}

case "$1" in
	""|table)
		compile_table_for_documentation ;;
	awk-table)
		compile_awk_tables ;;
	help|--help)
		show_help
		;;
	*)
		show_help
		exit 1
		;;
esac
