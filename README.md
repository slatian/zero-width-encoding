# Zero Width Encoding

This project aims to encode text using non-printable characters from Unicode. While these could be used for some very primitive Steganography the intended use is to get some hidden information through menus like `dmenu`, `bemenu` and `fzf` to make it possible to decouple the displayed text from the commands it triggers.

## Example scenario

Lets say we have a menu that should look a bit like this:
```
⭐ Favorite Notes
⭐ Scratchpad
📔 More Notes
✅ My Todo List (21/NaN)
Close Menu
```

With a simple application this is n problem, just grab onto the text and it will somehow work out, but maybe the names are provided by the user or a translation. Matching those is still possible, but done properly requires keeping the list of the exact display along with the commands.

To solve this one could prefix more machine friendly commands. ([see example.txt](example.txt))
```
list/fav,⭐ Favorite Notes
open/scratchpad,⭐ Scratchpad
list,📔 More Notes
todo/3,✅ My Todo List (21/NaN)
close,Close Menu
```

Those are more script friendly, but ugly for human tastes, but see what happens when we pipe it through [zwe.sh](zwe.sh)

```sh
cat example.txt | ./zwe.sh -e -k , | fzf | ./zwe.sh -d
```
The output is … one of those strings before the `,` delimiter you didn't see in the menu.

Happy simplifying!

<b>Note:</b> If you know your script well enough you can of course use a simpler version of this. (Remember to document it)

## Rules

Used Unicode characters:
* `_`: Zero Width Non-Joiner (U+200C)
* `j`: Zero Width Joiner (U+200D)
* `n`: Non-Breaking Zero width space (U+FEFF)
* `w`: Word Joiner (U+2060)
* `z`: Zero Width Space (U+200B)

<b>Note:</b> The U+0083 No Break here was also considered, but it has broken rendering with `bemenu`.

* An encoded symbol always starts with an `_`.
* An encoded sequence starts with a `z` to prevent the first `_` from interacting with whatever came before.
* A `_z` should terminate the sequence
* Symbol identifiers are generated by using `j`, `n`, `z` and `w` using every possible combination.

A custom character table is used to make encoding a tiny bit more efficient as every symbol takes at least 6 bytes to encode.

<b>Example:</b> The text "hello world!" would be encoded as `z_zz_zj_jjz_jjz_jnw_nwj_jzw_jnw_jwn_jjz_wz_nwn_z`. Please be aware that this inflates to 145 bytes!

## Symbol Table

| #   | Encoded | Char  |
|----:|--------:|-------|
|   0 | `    j` | 0     |
|   1 | `    n` | 1     |
|   2 | `    w` | 2     |
|   3 | `    z` | \n    |
|   4 | `   jj` | 3     |
|   5 | `   jn` | 4     |
|   6 | `   jw` | 5     |
|   7 | `   jz` | 6     |
|   8 | `   nj` | 7     |
|   9 | `   nn` | 8     |
|  10 | `   nw` | 9     |
|  11 | `   nz` | /     |
|  12 | `   wj` | a     |
|  13 | `   wn` | b     |
|  14 | `   ww` | c     |
|  15 | `   wz` | d     |
|  16 | `   zj` | e     |
|  17 | `   zn` | f     |
|  18 | `   zw` | g     |
|  19 | `   zz` | h     |
|  20 | `  jjj` | i     |
|  21 | `  jjn` | j     |
|  22 | `  jjw` | k     |
|  23 | `  jjz` | l     |
|  24 | `  jnj` | m     |
|  25 | `  jnn` | n     |
|  26 | `  jnw` | o     |
|  27 | `  jnz` | p     |
|  28 | `  jwj` | q     |
|  29 | `  jwn` | r     |
|  30 | `  jww` | s     |
|  31 | `  jwz` | t     |
|  32 | `  jzj` | u     |
|  33 | `  jzn` | v     |
|  34 | `  jzw` | w     |
|  35 | `  jzz` | x     |
|  36 | `  njj` | y     |
|  37 | `  njn` | z     |
|  38 | `  njw` | +     |
|  39 | `  njz` | _     |
|  40 | `  nnj` | -     |
|  41 | `  nnn` | .     |
|  42 | `  nnw` | :     |
|  43 | `  nnz` | =     |
|  44 | `  nwj` | SPACE |
|  45 | `  nwn` | !     |
|  46 | `  nww` | ?     |
|  47 | `  nwz` | #     |
|  48 | `  nzj` |       |

Version: 4.1 (2023-07-08)

## License

### README

This README is licensed under a [Creative Commons BY-SA 4.0 International license](https://creativecommons.org/licenses/by-nc-sa/4.0/).

© 2023 Slatian

### zwe.sh

The included [zwe.sh](zwe.sh) file is licensed under a [GPL-3.0-or-later](https://www.gnu.org/licenses/gpl-3.0.html) license.

