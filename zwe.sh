#!/bin/sh

# zwe.sh, A bash and awk implementation of the Zero Width Encoding
# Copyright (C) 2023 Slatian
#
# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.


# Make the script fail when a command fails
set -e

# Define the UTF-8 sequences used for encoding
GZ='\xE2\x80\x8B'
GN='\xEF\xBB\xBF'
GJ='\xE2\x80\x8D'
GW='\xe2\x81\xa0'
G_='\xE2\x80\x8C'

# Needed to make sure that thre is no unneccessary interpretion of bytes
export LANG=C


COMMAND=""
VISUAL=""
KEEP_AFTER=""

show_help() {
cat <<EOF
Usage: zwe <command> [--visual] [--keep-after <string>]

This script reads from stdin and outputs to stdout.

COMMANDS

-e, --encode
	Plain text to zero-width-encoding.
	
-d, --decode
	Extract zero-width-encoding and display as plain text.

-x, --extract
	Extract zero-width-encoding, but don't decode to plaintext.
	When used with --visual it outputs the ASCII representation.

-a, --assemble
	Convert the ASCII representation to the zero-width one.

OPTIONS

-v, --visual
	Instead of encoding to invisible characters enode to ascii representation.
	This will disable automatic extraction for decoding.
	Useful for debugging.
	
-k, --keep-after <string>
	When encoding everything that comes after the given character sequence
	will be kept as plaintext, the sequence itself will be discarded.
	

Enocdes or decodes a zero width code.

This version supports the following characters: _ j n z w

EOF
}

while [ "$#" -gt 0 ]; do
	case "$1" in
		--encode|-e) COMMAND="enc"; shift 1;;
		--decode|-d) COMMAND="dec"; shift 1;;
		--extract|-x) COMMAND="extract"; shift 1;;
		--assemble|-a) COMMAND="assemble"; shift 1;;

		--visual|-v) VISUAL="y"; shift 1;;
		--keep-after|-k) KEEP_AFTER="$2"; shift 2;;
		
		--help) show_help; exit 0;;
		*) printf "Unknown option: %s\n" "$1"; exit 1;;
	esac
done


visualize() {
	sed -e "s/$GZ/z/g" -e "s/$GN/n/g" -e "s/$GJ/j/g" -e "s/$GW/w/g" -e "s/$G_/\_/g"
}

unvisualize() {
 	sed -e "s/z/$GZ/g" -e "s/n/$GN/g" -e "s/j/$GJ/g" -e "s/w/$GW/g" -e "s/_/$G_/g"
}

extract() {
	grep -aoP "$GZ$G_($GZ|$GN|$GJ|$GW|$G_)*"
}

decode() {
	sed -e 's/^z_//' -e "s/_/\n/g" | awk  '
		BEGIN {
			dec_t["j"] = "0"; dec_t["n"] = "1"; dec_t["w"] = "2"; dec_t["z"] = "\n"; dec_t["jj"] = "3"; dec_t["jn"] = "4"; dec_t["jw"] = "5"; dec_t["jz"] = "6"; dec_t["nj"] = "7"; dec_t["nn"] = "8"; dec_t["nw"] = "9"; dec_t["nz"] = "/"; dec_t["wj"] = "a"; dec_t["wn"] = "b"; dec_t["ww"] = "c"; dec_t["wz"] = "d"; dec_t["zj"] = "e"; dec_t["zn"] = "f"; dec_t["zw"] = "g"; dec_t["zz"] = "h"; dec_t["jjj"] = "i"; dec_t["jjn"] = "j"; dec_t["jjw"] = "k"; dec_t["jjz"] = "l"; dec_t["jnj"] = "m"; dec_t["jnn"] = "n"; dec_t["jnw"] = "o"; dec_t["jnz"] = "p"; dec_t["jwj"] = "q"; dec_t["jwn"] = "r"; dec_t["jww"] = "s"; dec_t["jwz"] = "t"; dec_t["jzj"] = "u"; dec_t["jzn"] = "v"; dec_t["jzw"] = "w"; dec_t["jzz"] = "x"; dec_t["njj"] = "y"; dec_t["njn"] = "z"; dec_t["njw"] = "+"; dec_t["njz"] = "_"; dec_t["nnj"] = "-"; dec_t["nnn"] = "."; dec_t["nnw"] = ":"; dec_t["nnz"] = "="; dec_t["nwj"] = " "; dec_t["nwn"] = "!"; dec_t["nww"] = "?"; dec_t["nwz"] = "#";
			text="";
		}
		{
			text = text dec_t[$0]
		}
		END {
			printf("%s", text)
		}
	'
}

encode() {
	awk -v "keep_after=$KEEP_AFTER" \
		-v "GZ=$GZ" -v "GJ=$GJ" -v "GN=$GN" -v "GW=$GW" -v "G_=$G_" \
		-v "visual=$VISUAL" \
		'
		BEGIN {
			enc_t["0"] = "j"; enc_t["1"] = "n"; enc_t["2"] = "w"; enc_t["\n"] = "z"; enc_t["3"] = "jj"; enc_t["4"] = "jn"; enc_t["5"] = "jw"; enc_t["6"] = "jz"; enc_t["7"] = "nj"; enc_t["8"] = "nn"; enc_t["9"] = "nw"; enc_t["/"] = "nz"; enc_t["a"] = "wj"; enc_t["b"] = "wn"; enc_t["c"] = "ww"; enc_t["d"] = "wz"; enc_t["e"] = "zj"; enc_t["f"] = "zn"; enc_t["g"] = "zw"; enc_t["h"] = "zz"; enc_t["i"] = "jjj"; enc_t["j"] = "jjn"; enc_t["k"] = "jjw"; enc_t["l"] = "jjz"; enc_t["m"] = "jnj"; enc_t["n"] = "jnn"; enc_t["o"] = "jnw"; enc_t["p"] = "jnz"; enc_t["q"] = "jwj"; enc_t["r"] = "jwn"; enc_t["s"] = "jww"; enc_t["t"] = "jwz"; enc_t["u"] = "jzj"; enc_t["v"] = "jzn"; enc_t["w"] = "jzw"; enc_t["x"] = "jzz"; enc_t["y"] = "njj"; enc_t["z"] = "njn"; enc_t["+"] = "njw"; enc_t["_"] = "njz"; enc_t["-"] = "nnj"; enc_t["."] = "nnn"; enc_t[":"] = "nnw"; enc_t["="] = "nnz"; enc_t[" "] = "nwj"; enc_t["!"] = "nwn"; enc_t["?"] = "nww"; enc_t["#"] = "nwz";
		}
		/.+/{
			decoration=""
			until = length($0)+1
			if (keep_after) {
				until = index($0,keep_after)
				decoration = substr($0,until+1)
			}
			text="z";
			for (i=1;i<until;i++){
				text = text "_" enc_t[tolower(substr($0,i,1))]
			};
			text = text "_z"
			if (!visual) {
				gsub(/_/, G_, text)
				gsub(/w/, GW, text)
				gsub(/j/, GJ, text)
				gsub(/n/, GN, text)
				gsub(/z/, GZ, text)
			}
			print text decoration
		}
		/^$/{ print }
	'
}

case "$COMMAND" in
	enc)
		encode ;;
	dec)
		if [ -z "$VISUAL" ] ; then
			extract | visualize | decode
		else
			decode
		fi ;;
	extract)
		if [ -z "$VISUAL" ] ; then
			extract
		else
			extract | visualize
		fi ;;
	assemble)
		unvisualize ;;
	*)
		show_help
		exit 1
		;;
esac


